# Filter lists for adblocks

**Testarossa Ultralist** [|Subscribe|](https://subscribe.adblockplus.org/?location=https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/ultralisk.txt&title=Testarossa%20ultralist) [|View|](https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/ultralisk.txt)

Filter list designed to block elements, websites, providers or scripts that contain ads, trackers (online counters, web analytics tools...), annoying elements, unnecessary pop-ups, online scams or abusive behavior. Contains a blacklist that provides protection against sources of malware (domains and IPs). Created with a more comprehensive list, based on other good discontinued filter lists, optimized for uBlock Origin, to use as a supplementary list to the uBlock's, EasyList's and/or Adguard's lists.

**Testarossa anti-paywall** [|Subscribe|](https://subscribe.adblockplus.org/?location=https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/anti-paywall.txt&title=Testarossa%20anti-paywall) [|View|](https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/anti-paywall.txt)

Filter list to inhibit paywall methods that limit access to websites.

## Other filter lists:

### ANTI-ADVERTISING

**uBlock (RECOMMENDED):** https://cdn.statically.io/gh/uBlockOrigin/uAssets/master/filters/filters.txt
**Link 2:** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters.txt
**Link 3:** https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssets@master/filters/filters.txt
**Link 4:** https://ublockorigin.github.io/uAssets/filters/filters.txt

**AdGuard - Base:** https://filters.adtidy.org/extension/ublock/filters/2_without_easylist.txt

**AdGuard (RECOMMENDED):** https://filters.adtidy.org/extension/ublock/filters/2.txt 

**AdGuard - Mobile Ads (RECOMMENDED FOR MOBILE):** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_11_Mobile/filter.txt
**Link 2:** https://filters.adtidy.org/extension/ublock/filters/11.txt

**Easylist (RECOMMENDED):** https://easylist.to/easylist/easylist.txt
**Link 2:** https://secure.fanboy.co.nz/easylist.txt
**Link 3:** https://easylist-downloads.adblockplus.org/easylist.txt

**Nano:** https://raw.githubusercontent.com/NanoAdblocker/NanoFilters/master/NanoFilters/NanoBase.txt

**Pringent-Ads:** https://v.firebog.net/hosts/Prigent-Ads.txt

**AdNauseam:** https://cdn.statically.io/gh/dhowe/AdNauseam/master/filters/adnauseam.txt

### ANTI-TRACKING

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/privacy.txt
**Link 2:** https://cdn.statically.io/gh/uBlockOrigin/uAssets/master/filters/privacy.txt
**Link 3:** https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssets@master/filters/privacy.txt
**Link 4:** https://ublockorigin.github.io/uAssets/filters/privacy.txt

**AdGuard (RECOMMENDED):** https://filters.adtidy.org/extension/ublock/filters/3.txt
**Link 2:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_3_Spyware/filter.txt

**Easyprivacy (RECOMMENDED):** https://easylist.to/easylist/easyprivacy.txt
**Link 2:** https://secure.fanboy.co.nz/easyprivacy.txt
**Link 3:** https://easylist-downloads.adblockplus.org/easyprivacy.txt

**quidsup.net (RECOMMENDED):** https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt

**frogeye.fr:** https://hostfiles.frogeye.fr/firstparty-trackers-hosts.txt

**Abine DNT:** https://raw.githubusercontent.com/HxxxxxS/Blocklists/master/lists/Abine-DNT.txt

**Ultimate Privacy Filter:** https://filters.adavoid.org/ultimate-privacy-filter.txt

### ANTI-MALWARE

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/badware.txt
**Link 2:** https://cdn.statically.io/gh/uBlockOrigin/uAssets/master/filters/badware.txt
**Link 3:** https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssets@master/filters/badware.txt
**Link 4:** https://ublockorigin.github.io/uAssets/filters/badware.txt

**Urlhaus (RECOMMENDED):** https://gitlab.com/curben/urlhaus-filter/-/raw/master/urlhaus-filter-online.txt
**Link 2:** https://raw.githubusercontent.com/curbengh/urlhaus-filter/master/urlhaus-filter-online.txt
**Link 3:** https://cdn.jsdelivr.net/gh/curbengh/urlhaus-filter/urlhaus-filter-online.txt

**Dandelion Anti-malware (extensive):** https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Dandelion%20Sprout's%20Anti-Malware%20List.txt

**Pringent-Malware:** https://v.firebog.net/hosts/Prigent-Malware.txt

**Disconnect Simple Malvertising (obsolete):** https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt

**Ultimate Security Filter:** https://filters.adavoid.org/ultimate-security-filter.txt

### ANTI-SCAM

**Curben (RECOMMENDED):** https://phishing-filter.pages.dev/phishing-filter.txt

**Scam Blocklist by DurableNapkin (RECOMMENDED):** https://raw.githubusercontent.com/durablenapkin/scamblocklist/master/adguard.txt

**Joewein:** https://joewein.net/dl/bl/dom-bl-base.txt

**OpenPhish:** https://openphish.com/feed.txt

**Phishing Army (GOOD BUT EXTENSIVE):** https://phishing.army/download/phishing_army_blocklist_extended.txt

**emergingthreats.net:** https://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt

**firehol.org level 1 (RECOMMENDED):** https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level1.netset

**firehol.org level 2 (RECOMMENDED):** https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level2.netset

**firehol.org level 3 (RECOMMENDED):** https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level3.netset

**firehol.org level 4 (false positives):** https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level4.netset

**Spam404 (RECOMMENDED):** https://raw.githubusercontent.com/Spam404/lists/master/adblock-list.txt

**Cybercrime:** https://cybercrime-tracker.net/all.php

**Team CYMRU bogon list:** https://www.team-cymru.org/Services/Bogons/fullbogons-ipv4.txt 

**Ethanr DNS:** https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt

**FadeMind Risk:** https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Risk/hosts

**FadeMind Spam:** https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Spam/hosts

**Threat fox:** https://threatfox.abuse.ch/downloads/hostfile/

**Spamhaus Drop list:** https://www.spamhaus.org/drop/drop.txt

**Joewein blacklist base:** https://joewein.net/dl/bl/dom-bl-base.txt

**Joewein blacklist new:** https://joewein.net/dl/bl/dom-bl.txt

**DShield Blocklist:** https://www.dshield.org/ipsascii.html

**Pallebone:** https://raw.githubusercontent.com/pallebone/StrictBlockPAllebone/master/BlockIP.txt

**Cameleon:** http://sysctl.org/cameleon/hosts.win

**Anudeep's adservers:** https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt

**Emerging Block Ips:** https://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt

### ANTI-ANNOYANCES

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/annoyances.txt
**Link 2:** https://cdn.statically.io/gh/uBlockOrigin/uAssets/master/filters/annoyances.txt
**Link 3:** https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssets@master/filters/annoyances.txt
**Link 4:** https://ublockorigin.github.io/uAssets/filters/annoyances.txt

**AdGuard (RECOMMENDED):** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_14_Annoyances/filter.txt
**Link 2:** https://filters.adtidy.org/extension/ublock/filters/14.txt

**Fanboy (includes Fanboy-Social, Fanboy anti-cookie, Fanboy's Notifications):** https://easylist-downloads.adblockplus.org/fanboy-annoyance.txt
**Link 2:** https://secure.fanboy.co.nz/fanboy-annoyance.txt
**Link 3:** https://easylist.to/easylist/fanboy-annoyance.txt
**Link 4 (GOOD):** https://secure.fanboy.co.nz/fanboy-annoyance_ubo.txt

**Web Annoyances Ultralist:** https://raw.githubusercontent.com/yourduskquibbles/webannoyances/master/ultralist.txt
**Link 2:** https://cdn.jsdelivr.net/gh/yourduskquibbles/webannoyances/ultralist.txt

**Fanboy's Notifications Blocking List (included in Fanboy Annoyances):** https://easylist-downloads.adblockplus.org/fanboy-notifications.txt

### ANTI-COOKIE WARNINGS

**Fanboy (included in Fanboy Annoyances):** https://secure.fanboy.co.nz/fanboy-cookiemonster.txt

**I don't care about cookies:** https://www.i-dont-care-about-cookies.eu/abp/

**Probake (obsolete):** https://raw.githubusercontent.com/liamja/Prebake/master/obtrusive.txt

### ANTI-SOCIAL MEDIA

**AdGuard:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_4_Social/filter.txt
**Link2:** https://filters.adtidy.org/extension/ublock/filters/4.txt

**Fanboy (included in Fanboy Annoyances):** https://easylist.to/easylist/fanboy-social.txt
**Link 2:** https://easylist-downloads.adblockplus.org/fanboy-social.txt
**Link 3:** https://secure.fanboy.co.nz/fanboy-social.txt

**Fanboy antifacebook:** https://secure.fanboy.co.nz/fanboy-antifacebook.txt

### LISTAS PARA PORTUGUÊS/BRASIL

**AdGuard (RECOMENDADA):** https://filters.adtidy.org/extension/ublock/filters/9.txt
**Link 2:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_9_Spanish/filter.txt

**Easylist:** https://easylist-downloads.adblockplus.org/easylistportuguese.txt?a=1

**EasylistBrasil (BOA MAS QUEBRA ALGUNS SITES):** https://raw.githubusercontent.com/easylistbrasil/easylistbrasil/filtro/easylistbrasil.txt

**Fanboy (obsoleto):** https://secure.fanboy.co.nz/fanboy-espanol.txt

### ALL-IN-ONE

**Fanboy Complete List (includes Easylist, Easyprivacy, Enhanced Trackers List):** https://secure.fanboy.co.nz/r/fanboy-complete.txt

**Fanboy Ultimate List (includes Easylist, Easyprivacy, Enhanced Trackers List and Annoyances List):** https://secure.fanboy.co.nz/r/fanboy-ultimate.txt

**AdGuard DNS (includes social media, privacy/spyware, mobile, EasyList and EasyPrivacy):** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_15_DnsFilter/filter.txt
**Link 2:** https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt

**Adaway (FOR MOBILE):** https://adaway.org/hosts.txt

**Peter Lowe's servers host (RECOMMENDED):** https://pgl.yoyo.org/adservers/serverlist.php?hostformat=adblockplus&showintro=1&mimetype=plaintext

**CPBL Filters:** https://raw.githubusercontent.com/bongochong/CombinedPrivacyBlockLists/master/cpbl-abp-list.txt

**Dan Pollock’s hosts (RECOMMENDED):** https://someonewhocares.org/hosts/hosts

**MVPS hosts: (good but outdated)** https://winhelp2002.mvps.org/hosts.txt

**StevenBlack (includes AdAway, hostVN, MVPS hosts, Dan Pollock, URLHaus, Peter Lowe's, and more):** https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts

**StevenBlack +fakenews+gambling+porn+social:** https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn-social/hosts

**Blocklistproject abuse, ads, crypto, drugs, everything, facebook, fraud, gambling, malware, phishing, piracy, porn, ransomware, redirect, scam, TikTok, torrent, tracking (very extensive):**
https://raw.githubusercontent.com/blocklistproject/Lists/refs/heads/master/adguard/everything-ags.txt


### ANTI-ADBLOCK WARNINGS

**Reek's Anti-Adblock Killer (obsolete):** https://raw.github.com/reek/anti-adblock-killer/master/anti-adblock-killer-filters.txt

**Nano Defender (RECOMMENDED):** https://raw.githubusercontent.com/NanoAdblocker/NanoFilters/master/NanoMirror/NanoDefender.txt

**Easylist Adblock Warning Removal List (RECOMMENDED):** https://easylist-downloads.adblockplus.org/antiadblockfilters.txt?a=1

### WHITELIST

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/unbreak.txt
**Link 2:** https://cdn.statically.io/gh/uBlockOrigin/uAssets/master/filters/unbreak.txt
**Link 3:** https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssets@master/filters/unbreak.txt
**Link 4:** https://ublockorigin.github.io/uAssets/filters/unbreak.txt

### ANTI COIN MINER

**NoCoin (RECOMMENDED):** https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt

**NoCoin hosts:** https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt

**Anudeep's CoinMiner:** https://raw.githubusercontent.com/anudeepND/blacklist/master/CoinMiner.txt

**Coin Blocker List:** https://zerodot1.gitlab.io/CoinBlockerLists/hosts

### CLEAN URL TRACKING

**AdGuard URL (RECOMMENDED):** https://filters.adtidy.org/extension/ublock/filters/17.txt
**Link 2:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_17_TrackParam/filter.txt

**Dandelion URL Shortener (RECOMMENDED):** https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt

### OTHER FILTERS

**Bypass Paywalls (RECOMMENDED):** https://gitflic.ru/project/magnolia1234/bypass-paywalls-clean-filters/blob/raw?file=bpc-paywall-filter.txt

**uBlock resource abuse:** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/resource-abuse.txt
**Link 2:** https://cdn.statically.io/gh/uBlockOrigin/uAssets/master/filters/resource-abuse.txt
**Link 3:** https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssets@master/filters/resource-abuse.txt
**Link 4:** https://ublockorigin.github.io/uAssets/filters/resource-abuse.txt

**uBlock Outsider Intrusion into LAN (RECOMMENDED):** https://cdn.statically.io/gh/uBlockOrigin/uAssets/master/filters/lan-block.txt
**Link 2:** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/lan-block.txt
**Link 3:** https://cdn.jsdelivr.net/gh/uBlockOrigin/uAssets@master/filters/lan-block.txt
**Link 4:** https://ublockorigin.github.io/uAssets/filters/lan-block.txt

**Nano contrib filter – Placeholder buster:** https://raw.githubusercontent.com/NanoAdblockerLab/NanoContrib/master/dist/placeholder-buster.txt
**Link 2:** https://cdn.jsdelivr.net/gh/NanoAdblockerLab/NanoContrib@master/dist/placeholder-buster.txt

**BarbBlock (obsolete):** https://paulgb.github.io/BarbBlock/blacklists/adblock-plus.txt

**ABP filters anti-circumvention ads:** https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt?a=1
**Link 2:** https://easylist-msie.adblockplus.org/abp-filters-anti-cv.txt?a=1

**Fanboy Thirdparty Fonts:** https://secure.fanboy.co.nz/fanboy-antifonts.txt

**uBlock Plus:** https://raw.githubusercontent.com/IDKwhattoputhere/uBlock-Filters-Plus/master/uBlock-Filters-Plus.txt

### PAGES WITH FILTER LISTS:

https://github.com/uBlockOrigin/uAssets/tree/master/filters

https://kb.adguard.com/en/general/adguard-ad-filters

https://github.com/AdguardTeam/FiltersRegistry/tree/master/filters

https://github.com/ryanbr

https://easylist.to/

https://adblockplus.org/subscriptions

https://adblockultimate.net/filters

https://www.fanboy.co.nz/

https://www.fanboy.co.nz/regional.html

https://firebog.net/

https://github.com/NanoAdblocker/NanoFilters/blob/master/GitCDN.MD

https://filterlists.com/

https://www.iblocklist.com/lists.php

https://github.com/maravento/blackweb

https://zeltser.com/malicious-ip-blocklists/

https://gitlab.com/testa-rossa/adblock_filter_lists
